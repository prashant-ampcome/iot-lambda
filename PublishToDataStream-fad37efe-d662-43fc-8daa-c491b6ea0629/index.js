var amqp = require('amqplib/callback_api');


async function takeAction(event) {
    if (event.meter_id == 's3')
        event.Apparent_Power_Total_VA = (event.Current_Avg_A * event.Voltage_LL_Avg_V * 1.732);
    return new Promise(function(resolve, reject) {
        amqp.connect('amqp://ampcome:Ampcome1@energy.x1platform.com', function(err, conn) {
            if (!err) {
                conn.createChannel(function(err, ch) {
                    if (!err) {
                        var ex = 'meter_data_fanout_ex';
                        var msg = {
                            "event": {
                                "device_time": new Date(event.device_time).getTime(),
                                "meter_id": event.meter_id,
                                "voltage_ab_v": event.Voltage_AB_V.toFixed(4),
                                "voltage_bc_v": event.Voltage_BC_V.toFixed(4),
                                "voltage_ca_v": event.Voltage_CA_V.toFixed(4),
                                "frequency_hz": event.Frequency_Hz.toFixed(4),
                                "current_avg": event.Current_Avg_A.toFixed(4),
                                "active_power_total_kw": event.Active_Power_Total_kW.toFixed(4),
                                "current_a_a": event.Current_A_A.toFixed(4),
                                "current_b_a": event.Current_B_A.toFixed(4),
                                "current_c_a": event.Current_C_A.toFixed(4),
                                "apparent_power_total_kva": (event.Apparent_Power_Total_VA / 1000).toFixed(4),
                                "power_factor_total": event.Power_Factor_Total_Original.toFixed(4)
                            }
                        };
                        // console.log(msg);
                        ch.assertExchange(ex, 'fanout', { durable: false, autoDelete: false });
                        ch.publish(ex, 'data', new Buffer(JSON.stringify(msg)));
                        // console.log(" [x] Sent %s:'%s'", JSON.stringify(msg));
                        setTimeout(function() {
                            conn.close();
                            resolve();
                        }, 500);
                    }
                });
            }
        });
    });
}


exports.handler = async(event) => {
    await takeAction(event);
};
