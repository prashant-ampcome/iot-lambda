const { Pool, Client } = require('pg');

var pool = new Pool({
    user: 'postgres',
    host: '172.104.60.89',
    database: 'iotdb',
    port: 6432,
})

async function createPool() {
    pool = new Pool({
        user: 'postgres',
        host: '172.104.60.89',
        database: 'iotdb',
        port: 6432,
    })
}

var query = null;

async function prepareData(event) {
    if (event.meter_id == 's3')
        event.Apparent_Power_Total_VA = (event.Current_Avg_A * event.Voltage_LL_Avg_V * 1.732);
    event.Power_Factor_Total = (event.Active_Power_Total_kW * 1000) / (event.Current_Avg_A * event.Voltage_LL_Avg_V * 1.732);
}

async function generateQuery(event) {
    let cols = "";
    for (let x = 0; x < Object.keys(event).length; x++) {
        cols = cols + Object.keys(event)[x] + ",";
    }
    cols = cols.replace(/\,$/, "");

    let vals = "";
    for (let x = 0; x < Object.values(event).length; x++) {
        vals = vals + "'" + Object.values(event)[x] + "',";
    }
    vals = vals.replace(/\,$/, "");

    query = "insert into schneider_new(" + cols + ") values(" + vals + ")";
    console.log(query);
}

async function executeQuery(query) {
    return new Promise(function(resolve, reject) {
        pool.query(query)
            .then(res => {
                console.log('Data inserted successfully ');
                query = null;
                // pool.end();
                resolve();
            })
            .catch(e => {
                console.log(e);
                query = null;
                reject();
            });
    })

}


async function insert_data(event) {
    return new Promise(async function(resolve, reject) {
        if (pool) {
            console.log('Old pool found !');
            await prepareData(event);
            await generateQuery(event);
            await executeQuery(query);
            resolve();
        }
        else {
            console.log("creating new pool as existing pool seems to be closed");
            await createPool();
            await prepareData(event);
            await generateQuery(event);
            await executeQuery(event);
            resolve();
        }
    });
}

exports.handler = async(event) => {
    try {
        await insert_data(event);
        console.log(event);
        // await pool.end();
    }
    catch (e) {
        console.log(e);
    }
};
